# WorkFlow pour Wordpress

This projects documents and hosts a complete workflow to create and deploy custom wordpress images distributed by Docker.

It deploys wordpress, a database, a nginx server configured as reverse proxy, and certbot to configure connection using https.

For the wordpress part, the docker-compose.yml file install some plugins, and create a child theme for Divi.

## Deploy a new project

1. Copy `env.sample` to `.env`
2. Modify the values in `.env` 
3. Replace `my-project` in `docker-compose.yml` by whatever you want.
4. Deploy the stack with docker-compose up -d

## Modify Divi Child Theme

### Files structure

- `${WORDPRESS_DATADIR}/wp-content/themes/DiviFR` folder:  
  contains all files you need to modify Divi child-theme.
  - `style.css`:  itsources `style.css` from Divi parent theme, you can write css customizations in it.
  - `lang`: folder which contains translations files for Divi child theme in french.

### Translate Divi

1. Go to `${WORDPRESS_DATADIR}/wp-content/themes/DiviFR` folder
2. Open `lang/*/*.po files in a po editor
3. Add or modify translations
4. Generate `.po` et `.mo` files
5. Name them `fr_FR.po` and `fr_FR.mo`
6. Move them to their respective folder (`lang/theme` or `lang/builder`)
7. If you want to keep a backup of the modified child theme, just zip it to DiviFR.zip at the project root, you'll just have to use it 
when you deploy a new project.

