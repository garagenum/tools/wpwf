#!/bin/sh
sleep 20 ; \
echo " FULL_URL is : ${FULL_URL}" ; \
if ! $(wp core is-installed); then
    wp core install --path="/var/www/html"  --url=${FULL_URL} --title="${WEBSITE_TITLE}" --admin_user=${ADMIN_USER} --admin_password=${ADMIN_PASSWORD} --admin_email=${ADMIN_EMAIL} &&
    wp site empty --yes && wp post create --post_type=page --post_status=publish --post_title="Accueil" && 
    wp option update page_on_front 1 && wp option update show_on_front page  && 
    wp post create --post_type=page --post_status=draft --post_title="Politique de confidentialité" && 
    wp option update wp_page_for_privacy_policy 2 # && wp option update blog_public 0 &&
    wp language core install fr_FR &&
    wp site switch-language fr_FR ;
    wp plugin install wordfence &&
    wp plugin activate wordfence ;
    wp plugin install contact-form-7 &&
    wp plugin activate contact-form-7 ;
    wp plugin delete akismet hello ;
    wp theme install /tmp/Divi.zip &&
    wp theme install /tmp/DiviFR.zip &&
    wp theme activate DiviFR
fi
