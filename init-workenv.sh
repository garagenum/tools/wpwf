#!/bin/bash

no_env(){
    clear -x && cat << EOF2

Error: Missing '.env' file !
Create one or rename 'env.sample' to '.env'
and adapt it

EOF2
    false ; return
}

confirm_wordpress_creds(){
    ADMIN_USER=$(grep -oP '^ADMIN_USER=\K.*' .env)
    ADMIN_PASSWORD=$(grep -oP '^ADMIN_PASSWORD=\K.*' .env)
    echo " " 
    read -p "Entrez votre nom d'utilisateur Wordpress avec les droits admin: " NAME 
    read -s -p "Entrez votre mot de passe: " PASSWORD 
    [[ "$NAME" == "$ADMIN_USER" ]] && [[ "$PASSWORD" == "$ADMIN_PASSWORD" ]]
}

deploy(){
    mkdir -p wp_data certbot db_data 
    echo "Deploying now, you will need administrative rights with sudo " 
    sudo chown -R 33:33 wp_data || return 
    sudo chown -R 999:root db_data || return
    docker-compose down -v --remove-orphans
    docker-compose up -d
    #sudo sed -i "s/127.0.0.1/127.0.0.1  $VIRTUAL_HOST /" /etc/hosts
}

#
# In this vortex of functions, $1 is always the key in .env file, 
# $2 is the old value , $3 is the new value
# 
update_host(){
    [[ ! -z $DEBUG ]] && echo " ${FUNCNAME[0]} : " && for args in "$@" ; do echo $args ; done
    NEW_HOST=$(grep -oP "^$1=\K.*" .env)
    [[ ! -z $NEW_HOST ]] && OLD_HOST=$NEW_HOST  || ( echo "miss $1=some-value in '.env'" && exit )
    echo " "
    read -p " Do you want to keep \"$NEW_HOST\" as $1? y/N : " SAME
    [[ $SAME == "y" ]] && confirm_host $1 $OLD_HOST $NEW_HOST || set_host $1 $OLD_HOST
}

confirm_host(){
    [[ ! -z $DEBUG ]] && echo " ${FUNCNAME[0]} : " && for args in "$@" ; do echo $args ; done
    clear -x && echo " "
    read -p  "Defined $1 is $3. Confirm y/N : " KEEP
    [[ $KEEP == "y" ]] && set_files $1 $2 $3 || set_host $1 $2 
}

set_host(){
    [[ ! -z $DEBUG ]] && echo " ${FUNCNAME[0]} : " && for args in "$@" ; do echo $args ; done
    echo " "
    read -p  "Enter the new $1 and press Enter : " NEW_HOST
    [[ ! -z $NEW_HOST ]] && confirm_host $1 $2 $NEW_HOST || ( echo "No $1 value proposed, please retry. " ; set_host $1 $2)
}

set_files(){ 
    [[ ! -z $DEBUG ]] && echo " ${FUNCNAME[0]} : " && for args in "$@" ; do echo $args ; done
    sed -i "s,$2,$3,g" .env 
    sed -i "s,$2,$3,g" docker-compose.yml
    echo " "
    echo "$1 is now $3" 
}

main(){
    [[ ! -z $DEBUG ]] && echo " ${FUNCNAME[0]} : " && for args in "$@" ; do echo $args ; done
    clear -x 
    if ! [[ -f .env ]] ; then 
        no_env
    else 
        sleep 1 && update_host VIRTUAL_HOST && deploy 
    fi
    if ! [[ $? == 0 ]] ; then
        echo "No deployment"
    fi
}

DEBUG=""
main ;
